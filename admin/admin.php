<?php 

if ( !defined( 'ABSPATH' ) ) { exit; }

/**
* 
*/
class wp_sso_server_admin
{
	
	function __construct() {
		if (!is_admin()) { return; }
		add_action( 'admin_menu', array($this, 'menu') );
		add_action( 'admin_init', array($this, 'register_settings') );
	}

	function menu() {
		add_users_page( "Sitios habilitados", "WP SSO Server", 'promote_users', 'wp_sso_server', array($this, 'page_admin'));
	}

	function register_settings() {
		register_setting( 'wp_sso_server_admin_group', 'sites', array('sanitize_callback' => array($this, 'add_site') ));	
		//register_setting( 'wp_sso_server_sites',       'list');	
	}

	function page_admin() {

		if ( current_user_can( 'promote_users' ) && (isset($_GET['wp_sso_server_action']) ) ){
			$this->actions_save();
		}
		$data = array();
		$data['sites'] = get_option('wp_sso_server_sites', array());
		require realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'option_page.php';
	}

	function actions_save() {
		switch($_GET['wp_sso_server_action']) {
			case 'delete':
				if (isset($_GET['site_id'])) {
					$tmp = array();
					$sites = get_option('wp_sso_server_sites', array());	

					foreach ($sites as $site_id => $site) {
						if ($site_id != $_GET['site_id']) {
							$tmp[$site_id] = $site;
						}
					}
					delete_option('wp_sso_server_sites');
					add_option('wp_sso_server_sites',$tmp, '', true);

				}
			break;
		}		
	}

	function genHashSiteID($k)    { return wp_hash(SECURE_AUTH_KEY . $k . time() . md5($k), $k); }
	function genHashSiteToken($k) { return wp_hash(SECURE_AUTH_KEY . $k . time() . md5(md5($k)), $k); }

	function add_site() {
		$url = $_POST['url-site'];
		$sites = get_option('wp_sso_server_sites', array());

		foreach ($sites as $site) {
			if ($site['url'] == $url) {
				return '';
			}
		}

		$sites[ $this->genHashSiteID($url) ] = array(
			'url'      => $url,
			'token'    => $this->genHashSiteToken($url),
		);

		delete_option('wp_sso_server_sites');
		add_option('wp_sso_server_sites',$sites, '', true);
	}
}
