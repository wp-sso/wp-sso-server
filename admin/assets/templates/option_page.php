<?php 
if ( !defined( 'ABSPATH' ) ) { exit; }
?>
<div class="wrap">
	<h1 class="wp-heading-inline"><?php esc_html_e("Sitios habilitados", 'wp-sso-server'); ?></h1>
	<div>
		<?php esc_html_e("Revisa la documentación en ", 'wp-sso-server'); ?> <a href="https://gitlab.com/wp-sso/wp-sso-server" target="_blank">https://gitlab.com/wp-sso/wp-sso-server</a>
	</div>
	<br>
	<hr class="wp-header-end">
	<div id="col-container" class="wp-clearfix">
		<div id="col-left">
			<div class="col-wrap">
				<form method="post" action="options.php" style="clear: both">
					<h2><?php _e('Add Site', 'wp-sso-server') ?></h2>
					<?php settings_fields('wp_sso_server_admin_group'); ?>
					<input type="hidden" name="_wp_sso_server_action_add_site" value="wp_sso_server_admin_group">
					<div class="form-field form-required term-name-wrap">
						<label for="url-site"><?php _e('Site url', 'wp-sso-server') ?></label>
						<input name="url-site" id="url-site" type="text" value="" size="40" aria-required="true">
					</div>

					<p class="submit">
						<input type="submit" class="button-primary" value="<?php esc_html_e('Add', 'wp-sso-server') ?>" style="display: block;width: 96%;" />
					</p>
				</form>

			</div>
		</div><!-- /col-left -->

		<div id="col-right">
			<div class="col-wrap">
				<table class="wp-list-table widefat fixed striped">
					<thead>
						<tr>
							<th scope="col" id="name" class="manage-column column-name column-primary"><?php _e('Site', 'wp-sso-server') ?></th>
							<th scope="col" id="tokenid" class="manage-column column-token-id"><?php _e('Site ID', 'wp-sso-server') ?></th>
							<th scope="col" id="token" class="manage-column column-token"><?php _e('Token Secret', 'wp-sso-server') ?></th>
						</tr>
					</thead>
					<tbody id="the-list">
						<?php if(isset($data['sites']) && count($data['sites'])): foreach ($data['sites'] as $site_id => $site): ?>
							<tr>
								<td class="name column-name has-row-actions column-primary" data-colname="Sitio">
									<strong>
										<a class="row-title" href="<?php ?>" target="_blank"><?php echo $site['url']; ?></a>
									</strong>
									<br>
									<div class="row-actions">
										<span class="edit"><a href="?page=wp_sso_server&wp_sso_server_action=delete&site_id=<?php echo $site_id; ?>" aria-label="<?php _e('Delete site', 'wp-sso-server') ?>"><?php esc_html_e('Delete', 'wp-sso-server') ?></a> | </span>
										<span class="view"><a href="<?php echo $site['url']; ?>" target="_blank" aria-label="<?php _e('GO the site', 'wp-sso-server') ?>"><?php esc_html_e('GO', 'wp-sso-server') ?></a></span>
									</div>
								</td>
								<td class="name column-id" data-colname="ID"><?php echo $site_id; ?></td>
								<td class="name column-token" data-colname="Token"><?php echo $site['token']; ?></td>
							</tr>
						<?php endforeach; endif; ?>
					</tbody>
				</table>
			</div>
		</div><!-- /col-right -->
	</div>
</div>

